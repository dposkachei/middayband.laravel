<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConcertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concerts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('area_id');
            $table->timestamp('begin_at');
            $table->string('title');
            $table->text('description')->nullable()->default(null);
            $table->text('support')->nullable()->default(null);
            $table->string('image')->nullable()->default(null);
            $table->string('vk_event')->nullable()->default(null);
            $table->integer('price')->nullable()->default(null);
            $table->string('price_description')->nullable()->default(null);
            $table->tinyInteger('status')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('concerts', function (Blueprint $table) {
            $table->dropForeign('area_id');
        });
        Schema::dropIfExists('concerts');
    }
}
