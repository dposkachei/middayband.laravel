<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReleasePlaylistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('release_playlists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('release_id');
            $table->string('vk_playlist')->nullable()->default(null);
            $table->string('vk_owner_id')->nullable()->default(null);
            $table->string('vk_hash')->nullable()->default(null);
            $table->integer('priority')->unsigned()->default(0);
            $table->tinyInteger('status')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('release_id')->references('id')->on('releases')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('release_playlists', function (Blueprint $table) {
            $table->dropForeign('release_id');
        });
        Schema::dropIfExists('release_playlists');
    }
}
