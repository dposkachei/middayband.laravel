<?php

use Illuminate\Database\Seeder;

class ConcertsTableSeeder extends Seeder
{
    use \App\Database\Seeds\CommonDatabaseSeeder;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $aConcerts = [
            [
                'title' => 'DE LIBERTAS | 4 ГОДА СВОБОДЫ',
                'description' => 'За плечами огромная работа над собой, полная отдача и бесконечный драйв. 4 года DE LIBERTAS неустанно идут к своей мечте. Эту прекрасную дату они отметят в кругу друзей, приходите оторваться под любимые хиты!',
                'support' => 'Existory, IQ 50, Midday',
                'begin_at' => '20.01.2019',
                'address' => 'проспект Сельмаш, 3, Ростов-на-Дону',
                'vk_event' => 190692344,
                'price' => 300,
                'price_description' => 'С репостом 200р. (показать репост на входе)',
                'area' => [
                    'title' => 'Podzemka',
                    'phone' => '229-87-56',
                    'address' => 'проспект Сельмаш, 3, Ростов-на-Дону',
                    'place' => 'Rostov-on-Don. Podzemka Club',
                ],
            ],
        ];

        foreach ($aConcerts as $key => $aConcert) {
            $oArea = \App\Models\Area::where('title', $aConcert['area']['title'])->first();
            if (is_null($oArea)) {
                $oArea = \App\Models\Area::create($aConcert['area']);
            }
            \App\Models\Concert::create([
                'area_id' => $oArea->id,
                'begin_at' => \Carbon\Carbon::parse($aConcert['begin_at']),
                'title' => $aConcert['title'],
                'description' => $aConcert['description'],
                'support' => $aConcert['support'],
                'vk_event' => $aConcert['vk_event'],
                'image' => env('IMAGE_URL') . '/img/29.02.2020.jpg',
                'price' => $aConcert['price'],
                'price_description' => $aConcert['price_description'],
            ]);
        }
    }
}
