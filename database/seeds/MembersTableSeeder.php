<?php

use Illuminate\Database\Seeder;

class MembersTableSeeder extends Seeder
{
    use \App\Database\Seeds\CommonDatabaseSeeder;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'image' => env('IMAGE_URL') . '/img/dan.jpg',
                'title' => 'ДАНИИЛ',
                'description' => null,
                'role' => null,
                'size' => 'Бас',
            ], [
                'id' => 2,
                'image' => env('IMAGE_URL') . '/img/ar.jpg',
                'title' => 'АРТУР',
                'description' => null,
                'role' => null,
                'size' => 'Гитара',
            ], [
                'id' => 3,
                'image' => env('IMAGE_URL') . '/img/roma.jpg',
                'title' => 'РОМАН',
                'description' => null,
                'role' => null,
                'size' => 'Вокал',
            ], [
                'id' => 4,
                'image' => env('IMAGE_URL') . '/img/me.jpg',
                'title' => 'ДМИТРИЙ',
                'description' => null,
                'role' => null,
                'size' => 'Гитара',
            ], [
                'id' => 5,
                'image' => env('IMAGE_URL') . '/img/tem.jpg',
                'title' => 'АРТЕМ',
                'description' => null,
                'role' => null,
                'size' => 'Ударные',
            ],
        ];

        $total = count($data);
        foreach ($data as $key => $value) {
            \App\Models\Member::create([
                'title' => $value['title'],
                'description' => $value['description'],
                'role' => $value['role'],
                'size' => $value['size'],
                'image' => $value['image'],
                'priority' => $this->priority($key, $total),
            ]);
        }
    }
}
