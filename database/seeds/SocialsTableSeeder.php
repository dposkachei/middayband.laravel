<?php

use Illuminate\Database\Seeder;

class SocialsTableSeeder extends Seeder
{
    use \App\Database\Seeds\CommonDatabaseSeeder;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $aSocials = [
            [
                'title' => 'facebook',
                'color' => 'facebook',
                'name' => 'facebook',
                'url' => '/',
            ], [
                'title' => 'vk',
                'color' => 'vk',
                'name' => 'vk',
                'url' => '/',
            ], [
                'title' => 'instagram',
                'color' => 'instagram',
                'name' => 'instagram',
                'url' => '/',
            ], [
                'color' => 'youtube',
                'title' => 'youtube',
                'name' => 'youtube',
                'url' => '/',
            ],
        ];

        $total = count($aSocials);
        foreach ($aSocials as $key => $value) {
            \App\Models\Social::create([
                'color' => $value['color'],
                'name' => $value['name'],
                'url' => $value['url'],
                'title' => $value['title'],
                'priority' => $this->priority($key, $total),
            ]);
        }
    }
}
