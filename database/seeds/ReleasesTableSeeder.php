<?php

use Illuminate\Database\Seeder;

class ReleasesTableSeeder extends Seeder
{
    use \App\Database\Seeds\CommonDatabaseSeeder;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $value = [
            'title' => 'RISE',
            'year' => '2019',
            'image' => env('IMAGE_URL') . '/img/milk.jpg',
            'label' => 'NEW',
            'vk_playlist' => '-2000688606',
            'vk_owner_id' => '3688606', // 3688606
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae omnis, debitis. Molestiae omnis, debitis.',
            'links' => [
                [
                    'title' => 'Вконтакте',
                    'icon' => 'vk',
                    'url' => '/',
                ], [
                    'title' => 'Spotify',
                    'icon' => 'spotify',
                    'url' => '/',
                ], [
                    'title' => 'Itunes',
                    'icon' => 'itunes note',
                    'url' => '/',
                ], [
                    'title' => 'YouTube',
                    'icon' => 'youtube',
                    'url' => '/',
                ], [
                    'title' => 'Instagram',
                    'icon' => 'instagram',
                    'url' => '/',
                ], [
                    'title' => 'Patreon',
                    'icon' => 'patreon',
                    'url' => '/',
                ],
            ],
        ];
        $oRelease = \App\Models\Release::create([
            'title' => $value['title'],
            'year' => $value['year'],
            'label' => $value['label'],
            'description' => $value['description'],
            'image' => $value['image'],
            'priority' => 1,
        ]);

        $total = count($value['links']);
        foreach ($value['links'] as $key => $link) {
            \App\Models\ReleaseLink::create([
                'release_id' => $oRelease->id,
                'title' => $link['title'],
                'icon' => $link['icon'],
                'url' => $link['url'],
                'priority' => $this->priority($key, $total),
            ]);
        }
        \App\Models\ReleasePlaylist::create([
            'release_id' => $oRelease->id,
            'vk_playlist' => $value['vk_playlist'],
            'vk_owner_id' => $value['vk_owner_id'],
        ]);
    }
}
