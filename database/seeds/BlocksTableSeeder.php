<?php

use Illuminate\Database\Seeder;

class BlocksTableSeeder extends Seeder
{
    use \App\Database\Seeds\CommonDatabaseSeeder;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $aBlocks = [
            [
                'name' => 'music',
                'title' => 'МУЗЫКА',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
            ], [
                'name' => 'tours',
                'title' => 'КОНЦЕРТЫ',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'purpose' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, distinctio ducimus eius eligendi error fugiat id incidunt magni natus nulla, possimus provident, quo ullam voluptate voluptates. Asperiores atque minima sed.',
            ], [
                'name' => 'band',
                'title' => 'СОСТАВ',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
            ], [
                'name' => 'contacts',
                'title' => 'КОНТАКТЫ',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
            ],
        ];

        $total = count($aBlocks);
        foreach ($aBlocks as $key => $value) {
            \App\Models\Block::create([
                'name' => $value['name'],
                'title' => $value['title'],
                'description' => $value['description'],
                'purpose' => $value['purpose'] ?? null,
                'priority' => $this->priority($key, $total),
            ]);
        }
    }
}
