<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Block;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$fakerRU = \Faker\Factory::create('ru_RU');

$factory->define(Block::class, function (Faker $faker) use ($fakerRU) {
    return [
        'name' => $faker->sentence(),
        'title' => $fakerRU->sentence(),
        'description' => $fakerRU->realText(),
        'purpose' => $fakerRU->realText(),
        'priority' => $faker->randomDigit,
        'status' => 1,
    ];
});
