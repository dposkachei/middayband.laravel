<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Release;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$fakerRU = \Faker\Factory::create('ru_RU');

$factory->define(Release::class, function (Faker $faker) use ($fakerRU) {
    return [
        'title' => $fakerRU->sentence(),
        'year' => $faker->year,
        'label' => $faker->sentence(),
        'description' => $fakerRU->realText(),
        'image' => env('IMAGE_URL') . '/img/milk.jpg',
        'priority' => $faker->randomDigit,
        'status' => 1,
    ];
});
