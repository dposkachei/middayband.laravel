<?php

namespace App\VkontakteBot;

use Illuminate\Http\Request;

interface RequestTypeHandlerInterface
{
    /**
     * @param Request $request
     * @return mixed
     */
    public static function handle(Request $request);
}
