<?php

namespace App\VkontakteBot\BotKeyboard;

class ButtonRowFactory
{

    /**
     * @return ButtonRow
     */
    public static function createRow(): ButtonRow
    {

        return new ButtonRow();
    }
}
