<?php

namespace App\VkontakteBot\Response;

use App\VkontakteBot\BotKeyboard\Button;
use App\VkontakteBot\BotKeyboard\ButtonRowFactory;
use App\VkontakteBot\BotKeyboard\KeyboardFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use VK\Client\VKApiClient;

class ActionResponse
{

    private $vkApiClient;
    private $request;
    private $accessToken;
    private $botStandartMessages;
    private $botStandartAttachments;
    private $botButtonLabels;

    public function __construct(
        Request $request,
        VKApiClient $vkApiClient,
        String $accessToken
    )
    {
        $this->botStandartMessages = config('bot.messages');
        $this->botStandartAttachments = config('bot.media');
        $this->botButtonLabels = config('bot.buttons');
        $this->request = $request;
        $this->vkApiClient = $vkApiClient;
        $this->accessToken = $accessToken;
    }

//    public function start()
//    {
//        $params = [
//            'user_id' => $this->request->object['from_id'],
//            'random_id' => rand(0, 2 ** 31),
//            'message' => 'Привет чепушило, я тебя ждал',
//        ];
//
//        $this->vkApiClient->messages()->send($this->accessToken, $params);
//    }

    public function start()
    {
        $btnYes = Button::create(['button' => 'yes'], 'Да', 'primary');
        $btnNo = Button::create(['button' => 'no'], 'Нет', 'negative');

        $btnRow1 = ButtonRowFactory::createRow()
            ->addButton($btnYes)
            ->addButton($btnNo)
            ->getRow();

        $kb = KeyboardFactory::createKeyboard()
            ->addRow($btnRow1)
            ->setOneTime(false)
            ->getKeyboardJson();

        $params = [
            'user_id' => $this->request->object['from_id'],
            'random_id' => rand(0, 2 ** 31),
            'message' => 'Че ты пес, идешь на репу сегодня в 14?',
            'keyboard' => $kb,
        ];

        $this->vkApiClient->messages()->send($this->accessToken, $params);
    }
}
