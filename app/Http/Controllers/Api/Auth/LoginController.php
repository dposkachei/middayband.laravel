<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class LoginController extends Controller
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @param Request $request
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        return $this->authenticated($request);
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return array
     */
    public function loginByHash(Request $request, string $hash)
    {
        $this->user = $this->getUserByHash($hash);

        if (is_null($this->user)) {
            return responseCommon()->error();
        }

        return $this->authenticated($request);
    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @return array
     */
    protected function authenticated(Request $request)
    {
        $this->user->hash = Crypt::encrypt($this->user->user_email);

        return responseCommon()->success([
            'hash' => $this->user->hash,
        ], 'Вход успешно выполнен');
    }

    /**
     * @param string $hash
     * @return mixed
     */
    public function getUserByHash(string $hash)
    {
        $email = Crypt::decrypt($hash);
        return User::where('user_email', $email)->first();
    }
}
