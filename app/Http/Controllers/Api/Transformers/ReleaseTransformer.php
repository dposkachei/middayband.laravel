<?php

namespace App\Cmf\Api\Transformers;

use App\Models\Release;
use League\Fractal\TransformerAbstract;

class ReleaseTransformer extends TransformerAbstract
{
    /**
     * @param Release $oItem
     * @return array
     */
    public function transform(Release $oItem)
    {
        $oLinks = $oItem->links()->active()->ordered()->get();
        $oPlaylists = $oItem->playlists()->active()->ordered()->get();

        $aPlaylist = $oPlaylists->first();

        $data = [
            'title' => $oItem->title,
            'year' => $oItem->year,
            'image' => $oItem->image,
            'label' => $oItem->label,
            'vk_playlist' => $aPlaylist->vk_playlist,
            'vk_owner_id' => $aPlaylist->vk_owner_id, // 3688606
            'vk_hash' => $aPlaylist->vk_hash,
            'description' => $oItem->description,
        ];
        $data['links'] = $oLinks->toArray();
        return $data;
    }
}
