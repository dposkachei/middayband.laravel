<?php

namespace App\Cmf\Api\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * @param User $item
     * @return array
     */
    public function transform(User $item)
    {
        return [
            'id' => (int)$item->user_id,
            'email' => trim($item->user_email),
            'first_name' => trim($item->user_firstname),
            'last_name' => trim($item->user_lastname),
        ];
    }
}
