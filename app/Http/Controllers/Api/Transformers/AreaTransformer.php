<?php

namespace App\Cmf\Api\Transformers;

use App\Models\Area;
use App\Models\Concert;
use League\Fractal\TransformerAbstract;

class AreaTransformer extends TransformerAbstract
{
    /**
     * @param Area $item
     * @return array
     */
    public function transform(Area $item)
    {
        return [
            'id' => (int)$item->id,
            'title' => $item->title,
            'description' => $item->description,
            'phone' => $item->phone,
            'address' => $item->address,
            'place' => $item->place,
        ];
    }
}
