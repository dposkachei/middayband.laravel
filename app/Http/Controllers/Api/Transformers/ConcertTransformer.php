<?php

namespace App\Cmf\Api\Transformers;

use App\Models\Concert;
use League\Fractal\TransformerAbstract;

class ConcertTransformer extends TransformerAbstract
{
    /**
     * @param Concert $item
     * @return array
     */
    public function transform(Concert $item)
    {
        return [
            'id' => (int)$item->id,
            'area_id' => $item->area_id,
            'area' => (new AreaTransformer())->transform($item->area),
            'begin_at' => $item->begin_at->format('d.m.Y H:i:s'),
            'begin_at_format' => $item->begin_at->format('29/02/2020'),
            'title' => $item->title,
            'description' => $item->description,
            'support' => $item->support,
            'image' => $item->image,
            'vk_event' => $item->vk_event,
            'price' => $item->price,
            'price_description' => $item->price_description,
        ];
    }
}
