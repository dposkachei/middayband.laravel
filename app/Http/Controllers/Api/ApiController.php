<?php

namespace App\Http\Controllers\Api;

use App\Cmf\Api\Transformers\ConcertTransformer;
use App\Cmf\Api\Transformers\ReleaseTransformer;
use App\Cmf\Api\Transformers\UserTransformer;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Controller;
use App\Models\Block;
use App\Models\Concert;
use App\Models\Member;
use App\Models\Release;
use App\Models\Social;
use App\Models\Subscriber;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    use Helpers;

    /**
     * @return \Dingo\Api\Http\Response
     */
    public function index()
    {
        return $this->response->noContent();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function user(Request $request)
    {
        $controller = new LoginController();

        if ($request->has('hash') && $request->get('hash') !== 'null') {
            $result = $controller->loginByHash($request, $request->get('hash'));

            if (!$result['success']) {
                $this->response->error('Пользователь не авторизован.', 401);
            }

            $oUser = $controller->getUserByHash($request->get('hash'));

            $aItem = (new UserTransformer())->transform($oUser);

            return $this->response->array($aItem);
        }
        //$this->response->error('Пользователь не авторизован.', 401);

        return $this->response->array([]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function subscribe(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validation->fails()) {
            $this->response->error($validation->getMessageBag()->toArray()['email'][0], 422);
        }
        $oSubscriber = Subscriber::where('email', $request->get('email'))->first();
        if (!is_null($oSubscriber)) {
            $this->response->error('Такой email уже подписан на новости', 422);
        }
        Subscriber::create([
            'email' => $request->get('email'),
        ]);

        return $this->response->array([]);
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function data(Request $request)
    {
        $aMembers = [
            [
                'id' => 1,
                'image' => env('IMAGE_URL') . '/img/dan.jpg',
                'title' => 'ДАНИИЛ',
                'description' => '',
                'role' => '',
                'size' => 'size-s',
            ], [
                'id' => 2,
                'image' => env('IMAGE_URL') . '/img/ar.jpg',
                'title' => 'АРТУР',
                'description' => '',
                'role' => '',
                'size' => 'size-m',
            ], [
                'id' => 3,
                'image' => env('IMAGE_URL') . '/img/roma.jpg',
                'title' => 'РОМАН',
                'description' => '',
                'role' => '',
                'size' => 'size-xl',
            ], [
                'id' => 4,
                'image' => env('IMAGE_URL') . '/img/me.jpg',
                'title' => 'ДМИТРИЙ',
                'description' => '',
                'role' => '',
                'size' => 'size-l',
            ], [
                'id' => 5,
                'image' => env('IMAGE_URL') . '/img/tem.jpg',
                'title' => 'АРТЕМ',
                'description' => '',
                'role' => '',
                'size' => 'size-s',
            ],
        ];
        $aRelease = [
            'title' => 'RISE',
            'year' => '2019',
            'image' => env('IMAGE_URL') . '/img/milk.jpg',
            'label' => 'NEW',
            'vk_playlist' => '-2000688606',
            'vk_owner_id' => '3688606', // 3688606
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae omnis, debitis. Molestiae omnis, debitis.',
            'links' => [
                [
                    'title' => 'SPOTIFY',
                    'url' => '/',
                ], [
                    'title' => 'ITUNES',
                    'url' => '/',
                ], [
                    'title' => 'YOUTUBE',
                    'url' => '/',
                ],
            ],
        ];
        $aSocials = [
            [
                'color' => 'facebook',
                'name' => 'facebook',
                'url' => '/',
            ], [
                'color' => 'vk',
                'name' => 'vk',
                'url' => '/',
            ], [
                'color' => 'instagram',
                'name' => 'instagram',
                'url' => '/',
            ], [
                'color' => 'youtube',
                'name' => 'youtube',
                'url' => '/',
            ],
        ];
        $aImages = [
            'main' => env('IMAGE_URL') . '/img/band.jpg',
            'footer' => env('IMAGE_URL') . '/img/bandfooter.jpg',
        ];
        $aBlocks = [
            'music' => [
                'title' => 'МУЗЫКА',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
            ],
            'tours' => [
                'title' => 'КОНЦЕРТЫ',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                'purpose' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus, distinctio ducimus eius eligendi error fugiat id incidunt magni natus nulla, possimus provident, quo ullam voluptate voluptates. Asperiores atque minima sed.',
            ],
            'band' => [
                'title' => 'СОСТАВ',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
            ],
            'contacts' => [
                'title' => 'КОНТАКТЫ',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
            ],
        ];


        return $this->response->array([
            'data' => [
                'members' => $aMembers,
                'socials' => $aSocials,
                'release' => $aRelease,
                'images' => $aImages,
                'blocks' => $aBlocks,
            ],
        ]);
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function dataNew(Request $request)
    {
        $oMembers = Member::active()->ordered()->get();
        $aMembers = $oMembers->toArray();

        $oRelease = Release::active()->first();
        $aRelease = (new ReleaseTransformer())->transform($oRelease);


        $oSocials = Social::active()->ordered()->get();
        $aSocials = $oSocials->toArray();

        $aImages = [
            'main' => env('IMAGE_URL') . '/img/band.jpg',
            'footer' => env('IMAGE_URL') . '/img/bandfooter.jpg',
        ];

        $oBlocks = Block::active()->ordered()->get();
        $aBlocks = [];
        foreach ($oBlocks as $oBlock) {
            $aBlocks[$oBlock->name] = [
                'title' => $oBlock->title,
                'description' => $oBlock->description,
                'purpose' => $oBlock->purpose,
            ];
        }

        $aConcerts = Concert::active()->ordered()->get()->transform(function ($item) {
            return (new ConcertTransformer())->transform($item);
        })->all();

        return $this->response->array([
            'data' => [
                'members' => $aMembers,
                'socials' => $aSocials,
                'release' => $aRelease,
                'images' => $aImages,
                'blocks' => $aBlocks,
                'concerts' => $aConcerts,
            ],
        ]);
    }
}
