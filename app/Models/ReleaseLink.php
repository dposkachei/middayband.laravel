<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReleaseLink extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'release_id', 'title', 'icon', 'url', 'priority', 'status'
    ];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('priority', 'desc');
    }
}
