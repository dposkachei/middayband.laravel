<?php

use Dingo\Api\Routing\Router;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app(Router::class);

$api->version('v1', function ($api) {
    $api->group([
        'middleware' => ['api', 'api.auth'],
    ], function ($api) {
        $api->any('/', ['as' => 'index', 'uses' => \App\Http\Controllers\Api\ApiController::class . '@index']);
        $api->any('/user', ['as' => 'index', 'uses' => \App\Http\Controllers\Api\ApiController::class . '@user']);
        $api->any('/subscribe', ['as' => 'index', 'uses' => \App\Http\Controllers\Api\ApiController::class . '@subscribe']);
        $api->any('/dataOld', ['as' => 'index', 'uses' => \App\Http\Controllers\Api\ApiController::class . '@data']);
        $api->any('/data', ['as' => 'index', 'uses' => \App\Http\Controllers\Api\ApiController::class . '@dataNew']);

        $api->any('/vk/callback', ['as' => 'vk.callback', 'uses' => '\\' . \App\Http\Controllers\VkApiCallbackController::class . '@execute']);
    });
});
