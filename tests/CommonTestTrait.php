<?php

namespace Tests;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Queue;

trait CommonTestTrait
{
    /**
     * @var User
     */
    protected $oUser;

    /**
     * @param array $data
     * @param bool $ajax
     * @return Request
     */
    protected function request(array $data = [], bool $ajax = false): Request
    {
        $request = new Request();
        $request->merge($data);
        $request->merge([
            'phpunit' => true,
        ]);

        if ($ajax) {
            $request->headers->set('X-Requested-With', 'XMLHttpRequest');
        }

        return $request;
    }

    /**
     * @param string $url
     * @param array $data
     * @return mixed
     */
    protected function postAjax(string $url, array $data = [])
    {
        return $this->post($url, $data, ['HTTP_X-Requested-With' => 'XMLHttpRequest']);
    }

    /**
     * @param string $key
     * @param string $route
     * @param \Illuminate\Foundation\Testing\TestResponse|null $response
     * @param array $statuses 422 может быть только ошибка валидации, для успешного достижения метода этого достаточно
     */
    public function assertResponse($key, $route, ?\Illuminate\Foundation\Testing\TestResponse $response, array $statuses = [])
    {
        $statuses = array_merge($statuses, [200, 422, 404]);
        if (!is_null($response)) {
            $this->assertTrue(
                in_array($response->getStatusCode(), $statuses),
                $this->textRed('Error route ' . $key . '.' . $route . '. With status ' . $response->getStatusCode() . '.')
            );
        }
    }

    /**
     * @return \Illuminate\Support\Testing\Fakes\QueueFake
     */
    protected function queueFake(): \Illuminate\Support\Testing\Fakes\QueueFake
    {
        return (new Queue())->fake();
    }

    /**
     * @param mixed $object
     * @param string $methodName
     * @param array $parameters
     * @return mixed
     * @throws \ReflectionException
     */
    public function reflectionClass(&$object, string $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $parameters);
    }
}
