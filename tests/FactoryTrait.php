<?php

namespace Tests;

use App\Models\Block;
use App\Models\Concert;
use App\Models\Member;
use App\Models\Release;
use App\Models\Social;

trait FactoryTrait
{
    /**
     * @param array $data
     * @return Block
     */
    protected function factoryBlock(array $data = []): Block
    {
        return factory(Block::class)->create($data);
    }

    /**
     * @param array $data
     * @return Member
     */
    protected function factoryMember(array $data = []): Member
    {
        return factory(Member::class)->create($data);
    }

    /**
     * @param array $data
     * @return Release
     */
    protected function factoryRelease(array $data = []): Release
    {
        return factory(Release::class)->create($data);
    }

    /**
     * @param array $data
     * @return Social
     */
    protected function factorySocial(array $data = []): Social
    {
        return factory(Social::class)->create($data);
    }

    /**
     * @param array $data
     * @return Concert
     */
    protected function factoryConcert(array $data = []): Concert
    {
        return factory(Concert::class)->create($data);
    }
}
