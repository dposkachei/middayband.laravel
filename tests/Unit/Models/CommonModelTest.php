<?php

namespace Tests\Unit\Models;

use App\Models\Block;
use App\Models\Member;
use App\Models\Release;
use App\Models\Social;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\FactoryTrait;
use Tests\TestCase;

class CommonModelTest extends TestCase
{
    use DatabaseTransactions;
    use FactoryTrait;

    public function testBlock()
    {
        $oItem = $this->factoryBlock();
        $this->assertTrue(true);
    }

    public function testMember()
    {
        $oItem = $this->factoryMember();
        $this->assertTrue(true);
    }

    public function testSocial()
    {
        $oItem = $this->factorySocial();
        $this->assertTrue(true);
    }

    public function testRelease()
    {
        $oItem = $this->factoryRelease();
        $this->assertTrue(true);
    }

    public function testConcert()
    {
        $oItem = $this->factoryConcert();
        $this->assertTrue(true);
    }
}
