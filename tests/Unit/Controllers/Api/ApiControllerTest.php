<?php

namespace Tests\Unit\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\FactoryTrait;
use Tests\TestCase;

class ApiControllerTest extends TestCase
{
    use DatabaseTransactions;
    use FactoryTrait;

    /**
     * @return ApiController
     */
    public function controller(): ApiController
    {
        return new ApiController();
    }

    public function testIndex()
    {
        $response = $this->controller()->index();
        $this->assertEquals(204, $response->getStatusCode());
    }

    public function testUser()
    {
        $this->assertTrue(true);
    }

    public function testSubscribe()
    {
        // нет email
        try {
            $request = $this->request([]);
            $response = $this->controller()->subscribe($request);
        } catch (\Symfony\Component\HttpKernel\Exception\HttpException $e) {
            $this->assertEquals(422, $e->getStatusCode());
        }

        // есть email
        $request = $this->request([
            'email' => 'admin@admin.com'
        ]);
        $response = $this->controller()->subscribe($request);
        $this->assertEquals(200, $response->getStatusCode());

        // пытается второй раз подписаться
        try {
            $response = $this->controller()->subscribe($request);
        } catch (\Symfony\Component\HttpKernel\Exception\HttpException $e) {
            $this->assertEquals(422, $e->getStatusCode());
        }
    }

    public function testData()
    {
        $response = $this->controller()->data($this->request([]));
        $content1 = $response->getOriginalContent();

        $response = $this->controller()->dataNew($this->request([]));
        $content2 = $response->getOriginalContent();

        $this->assertNotEmpty($content2['data']);
        $this->assertNotEmpty($content2['data']['members']);
        $this->assertNotEmpty($content2['data']['socials']);
        $this->assertNotEmpty($content2['data']['release']);
        $this->assertNotEmpty($content2['data']['images']);
        $this->assertNotEmpty($content2['data']['blocks']);
        $this->assertNotEmpty($content2['data']['concerts']);
    }
}
